from flask import current_app as app
from . import basic_auth
from .forms import PasswordForm
from flask import request, render_template
from flask_admin import AdminIndexView, BaseView, expose
# from flask_admin.contrib.sqla import ModelView
import os
import flask
import google.oauth2.credentials
import google_auth_oauthlib.flow
import googleapiclient.discovery

# The path to the client-secrets.json file obtained from the Google API
# Console. You must set this before running this application.
CLIENT_SECRETS_FILENAME = os.environ['GOOGLE_CLIENT_SECRETS']
GOOGLE_CLIENT_ID = os.environ['GOOGLE_CLIENT_ID']
# The OAuth 2.0 scopes that this application will ask the user for. In this
# case the application will ask for basic profile information.
SCOPES = ['email', 'profile', 'https://www.googleapis.com/auth/youtube']


@app.route('/')
def index():
    if 'credentials' not in flask.session:
        return flask.redirect('authorize')

    # Load the credentials from the session.
    credentials = google.oauth2.credentials.Credentials(
        **flask.session['credentials'])

    # Get the basic user info from the Google OAuth2.0 API.
    client = googleapiclient.discovery.build(
        'oauth2', 'v2', credentials=credentials)

    response = client.userinfo().v2().me().get().execute()
    print(response)
    return render_template('app.html', client=GOOGLE_CLIENT_ID)


@app.route('/authorize')
def authorize():
    # Create a flow instance to manage the OAuth 2.0 Authorization Grant Flow
    # steps.
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        CLIENT_SECRETS_FILENAME, scopes=SCOPES)
    flow.redirect_uri = flask.url_for('oauth2callback', _external=True)
    authorization_url, state = flow.authorization_url(
        # This parameter enables offline access which gives your application
        # an access token and a refresh token for the user's credentials.
        access_type='offline',
        prompt='consent',
        # This parameter enables incremental auth.
        include_granted_scopes='true')

    # Store the state in the session so that the callback can verify the
    # authorization server response.
    flask.session['state'] = state

    return flask.redirect(authorization_url)


@app.route('/oauth2callback')
def oauth2callback():
    # Specify the state when creating the flow in the callback so that it can
    # verify the authorization server response.
    state = flask.session['state']
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        CLIENT_SECRETS_FILENAME, scopes=SCOPES, state=state)
    flow.redirect_uri = flask.url_for('oauth2callback', _external=True)

    # Use the authorization server's response to fetch the OAuth 2.0 tokens.
    authorization_response = flask.request.url
    flow.fetch_token(authorization_response=authorization_response)

    # Store the credentials in the session.
    credentials = flow.credentials
    flask.session['credentials'] = {
        'token': credentials.token,
        'refresh_token': credentials.refresh_token,
        'token_uri': credentials.token_uri,
        'client_id': credentials.client_id,
        'client_secret': credentials.client_secret,
        'scopes': credentials.scopes
    }
    print(flask.session)
    return flask.redirect(flask.url_for('index'))


@basic_auth.required
@app.route('/admin')
def admin_view():
    form = PasswordForm
    if request.method == "GET":
        return render_template('admin/login.html', form=form)


class AdminLoginView(AdminIndexView):
    @expose('/', methods=['GET', 'POST'])
    @app.route('/admin')
    def admin_index(self):
        return self.render('admin/index.html')


class AnalyticsView(BaseView):
    @expose('/')
    def admin_analytics_index(self):
        return self.render('admin/analytics_index.html')


'''
class TweetView(ModelView):
    @expose('/')
    form_create_rules = ('account', 'first_name', 'last_name')

'''

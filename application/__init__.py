from flask import Flask
from flask_basicauth import BasicAuth
basic_auth = BasicAuth()
''' 
For development - 
You need to make sure your FLASK_APP environment var is 'application'
Execute 'flask run' from inside the /basic module 
'''


def create_app():
    '''Flask Factory'''
    flask_app = Flask(__name__, instance_relative_config=True, static_folder='static')
    print('Uses .env and config.py for settings ')
    from werkzeug.utils import import_string
    cfg = import_string('basic.config.DevConfig')()
    flask_app.config.from_object(cfg)
    print(flask_app.config)
    basic_auth.init_app(flask_app)
    with flask_app.app_context():
        # flask_app.register_blueprint(basic_auth)
        # From application module
        from . import routes, admin
        print("Flask Factory Built")
        return flask_app


if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0', port=5000, debug=True)

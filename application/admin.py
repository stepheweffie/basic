from flask_admin import Admin
# from flask_admin.contrib.fileadmin import FileAdmin
# from flask_admin.menu import MenuLink
from .routes import AdminLoginView, AnalyticsView #TweetView
from flask import current_app as app
import os.path as op
# from application import db

path = op.join(op.dirname(__file__), 'templates')
admin = Admin(app, index_view=AdminLoginView(), template_mode='bootstrap3')
admin.add_view(AnalyticsView(name='Analytics', endpoint='analytics'))
#admin.add_view(TweetView(name='Tweets', endpoint='tweets'))

from wtforms import PasswordField, validators
from flask_wtf import Form


class PasswordForm(Form):
    password = PasswordField('Password', [validators.Length(min=5)])

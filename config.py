from os import environ


class DevConfig:
    TESTING = True
    DEBUG = True
    ENV = 'development'
    # use this to check your .env is loading vars
    SECRET_KEY = environ.get('SECRET_KEY')
    FLASK_APP = environ.get('FLASK_APP')
    JSONIFY_PRETTYPRINT_REGULAR = True
    FLASK_ADMIN_SWATCH = 'cerulean'
    BASIC_AUTH_USERNAME = environ.get('BASIC_AUTH_USERNAME')
    BASIC_AUTH_PASSWORD = environ.get('BASIC_AUTH_PASSWORD')
    CONSUMER_KEY = environ.get('API_KEY')
    CONSUMER_SECRET = environ.get('API_SECRET')

